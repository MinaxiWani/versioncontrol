DROP TABLE IF EXISTS `wp_revisr`;
CREATE TABLE `wp_revisr` (
  `id` mediumint(9) NOT NULL AUTO_INCREMENT,
  `time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `message` text,
  `event` varchar(42) NOT NULL,
  `user` varchar(60) DEFAULT NULL,
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
LOCK TABLES `wp_revisr` WRITE;
INSERT INTO `wp_revisr` VALUES ('1','2020-04-07 11:52:50','Successfully created a new repository.','init','versioncontrol'), ('2','2020-04-07 11:57:42','Successfully backed up the database.','backup','versioncontrol'), ('3','2020-04-07 11:57:45','Error pulling changes from the remote repository.','error','versioncontrol'), ('4','2020-04-07 11:59:41','Successfully backed up the database.','backup','versioncontrol'), ('5','2020-04-07 11:59:43','Error pulling changes from the remote repository.','error','versioncontrol'), ('6','2020-04-07 12:01:42','Successfully imported the database. <a href=\"http://localhost:8888/versioncontrol/wp-admin/admin-post.php?action=process_revert&amp;revert_type=db&amp;db_hash=4ed8608&amp;revisr_revert_nonce=1ba7e27e93\">Undo</a>','import','versioncontrol'), ('7','2020-04-07 12:02:20','Successfully pushed 0 commits to origin/master.','push','versioncontrol'), ('8','2020-04-07 12:03:10','Committed <a href=\"http://localhost:8888/versioncontrol/wp-admin/admin.php?page=revisr_view_commit&commit=9896d92&success=true\">#9896d92</a> to the local repository.','commit','versioncontrol'), ('9','2020-04-07 12:05:00','Successfully pushed 1 commit to origin/master.','push','versioncontrol'), ('10','2020-04-07 13:10:24','Successfully backed up the database.','backup','versioncontrol'), ('11','2020-04-07 13:10:45','Successfully pushed 1 commit to origin/master.','push','versioncontrol'), ('12','2020-04-07 13:19:14','Successfully pushed 0 commits to origin/master.','push','versioncontrol'), ('13','2020-04-07 13:19:14','Sent update request to the webhook.','push','versioncontrol');
UNLOCK TABLES;
